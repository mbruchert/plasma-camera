# Translation of plasma-camera to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-camera\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-17 00:47+0000\n"
"PO-Revision-Date: 2020-02-16 14:46+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.12.2\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#: src/contents/ui/CameraPage.qml:19 src/contents/ui/GlobalDrawer.qml:44
#: src/contents/ui/main.qml:47
#, kde-format
msgid "Camera"
msgstr "Kamera"

#: src/contents/ui/CameraPage.qml:31
#, kde-format
msgid "Switch mode"
msgstr "Byt modus"

#: src/contents/ui/CameraPage.qml:53
#, kde-format
msgid "Cancel self-timer"
msgstr "Avbryt sjølvutløysar"

#: src/contents/ui/CameraPage.qml:55
#, kde-format
msgid "Capture photo"
msgstr "Ta bilete"

#: src/contents/ui/CameraPage.qml:57
#, kde-format
msgid "Stop recording video"
msgstr "Stopp opptak"

#: src/contents/ui/CameraPage.qml:59
#, kde-format
msgid "Start recording video"
msgstr "Start opptak"

#: src/contents/ui/CameraPage.qml:93
#, kde-format
msgid "Switch Camera"
msgstr "Byt kamera"

#: src/contents/ui/CameraPage.qml:143
#, kde-format
msgid "Camera not available"
msgstr "Kamera er ikkje tilgjengeleg"

#: src/contents/ui/CameraPage.qml:145
#, kde-format
msgid "Camera is busy. Is another application using it?"
msgstr "Kameraet er oppteke. Kanskje i bruk av eit anna program?"

#: src/contents/ui/CameraPage.qml:147
#, kde-format
msgid "Missing camera resource."
msgstr "Manglar kameraressurs."

#: src/contents/ui/CameraPage.qml:283
#, kde-format
msgid "Took a photo"
msgstr "Tok eit bilete"

#: src/contents/ui/CameraPage.qml:286
#, kde-format
msgid "Failed to take a photo"
msgstr "Klarte ikkje ta bilete"

#: src/contents/ui/CameraPage.qml:292
#, kde-format
msgid "Stopped recording"
msgstr "Stoppa opptak"

#: src/contents/ui/CameraPage.qml:298
#, kde-format
msgid "Started recording"
msgstr "Starta opptak"

#: src/contents/ui/CameraPage.qml:301
#, kde-format
msgid "Failed to start recording"
msgstr "Klarte ikkje starta opptak"

#: src/contents/ui/GlobalDrawer.qml:61
#, kde-format
msgid "Resolution"
msgstr "Oppløysing"

#: src/contents/ui/GlobalDrawer.qml:81
#, kde-format
msgid "White balance"
msgstr "Kvitbalanse"

#: src/contents/ui/GlobalDrawer.qml:86
#, kde-format
msgid "Auto"
msgstr "Auto"

#: src/contents/ui/GlobalDrawer.qml:92
#, kde-format
msgid "Sunlight"
msgstr "Sollys"

#: src/contents/ui/GlobalDrawer.qml:98
#, kde-format
msgid "Cloudy"
msgstr "Overskya"

#: src/contents/ui/GlobalDrawer.qml:104
#, kde-format
msgid "Tungsten"
msgstr "Glødelampe"

#: src/contents/ui/GlobalDrawer.qml:110
#, kde-format
msgid "Fluorescent"
msgstr "Lysrøyr"

#: src/contents/ui/GlobalDrawer.qml:115
#, kde-format
msgid "Self-timer"
msgstr "Sjølvutløysar"

#: src/contents/ui/GlobalDrawer.qml:120
#, kde-format
msgid "Off"
msgstr "Av"

#: src/contents/ui/GlobalDrawer.qml:125
#, kde-format
msgid "2 s"
msgstr "2 s"

#: src/contents/ui/GlobalDrawer.qml:130
#, kde-format
msgid "5 s"
msgstr "5 s"

#: src/contents/ui/GlobalDrawer.qml:135
#, kde-format
msgid "10 s"
msgstr "10 s"

#: src/contents/ui/GlobalDrawer.qml:140
#, kde-format
msgid "20 s"
msgstr "20 s"

#: src/contents/ui/GlobalDrawer.qml:146
#, kde-format
msgid "About"
msgstr "Om"

#: src/contents/ui/main.qml:43
#, kde-format
msgid ""
"An error occurred: \"%1\". Please consider restarting the application if it "
"stopped working."
msgstr ""
"Det oppstod ein feil: «%1». Vurdera å starta programmet på nytt dersom det "
"har slutta å verka."

#: src/main.cpp:44
#, kde-format
msgid "© Plasma Mobile Developers"
msgstr "© Plasma Mobile-utviklarane"

#: src/main.cpp:46
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: src/main.cpp:47
#, kde-format
msgid "Jonah Brüchert"
msgstr "Jonah Brüchert"
